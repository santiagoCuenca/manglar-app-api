CREATE TABLE manglar_app.history_changes
(
    history_change_id integer NOT NULL,
    history_change_status boolean,
    user_id integer NOT NULL,
    history_change_date timestamp without time zone NOT NULL,
    form_id integer NOT NULL,
    form_type character varying(255)  NOT NULL,
    type_change character varying(255),
    user_name character varying(255),
    old_state character varying(355),
    new_state character varying(355),

    CONSTRAINT pk_history_changes PRIMARY KEY (history_change_id)
);

ALTER TABLE manglar_app.history_changes
    OWNER to postgres;

CREATE INDEX history_change_id ON manglar_app.history_changes USING btree (history_change_id);

CREATE SEQUENCE manglar_app.seq_history_changes
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;
