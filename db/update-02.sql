UPDATE suia_iii.roles SET role_description='ManglarApp-Socio', role_name='ManglarApp-Socio', role_sistemas='ManglarApp-Socio', role_used_authority='ManglarApp-Socio' WHERE role_name='Socio-Manglar';

UPDATE suia_iii.roles SET role_description='ManglarApp-Tecnico', role_name='ManglarApp-Tecnico', role_sistemas='ManglarApp-Tecnico', role_used_authority='ManglarApp-Tecnico' WHERE role_name='Socio-Manglar-Administrador';

UPDATE suia_iii.roles SET role_description='ManglarApp-Administrador', role_name='ManglarApp-Administrador', role_sistemas='ManglarApp-Administrador', role_used_authority='ManglarApp-Administrador' WHERE role_name='Socio-Manglar-Administrador-Super';

UPDATE manglar_app.organizations_manglar SET organization_manglar_type='super-admin' WHERE organization_manglar_type='org_socio';


INSERT INTO manglar_app.organizations_manglar(
	organization_manglar_id, organization_manglar_status, organization_manglar_name, organization_manglar_complete_name, organization_manglar_type)
	VALUES ((SELECT nextval('manglar_app.seq_organization_manglar_id')), true, 'MAE', 'MAE', 'admin');

INSERT INTO manglar_app.organizations_manglar(
	organization_manglar_id, organization_manglar_status, organization_manglar_name, organization_manglar_complete_name, organization_manglar_type)
	VALUES ((SELECT nextval('manglar_app.seq_organization_manglar_id')), true, 'MPCEIP', 'MPCEIP', 'admin');

INSERT INTO manglar_app.organizations_manglar(
	organization_manglar_id, organization_manglar_status, organization_manglar_name, organization_manglar_complete_name, organization_manglar_type)
	VALUES ((SELECT nextval('manglar_app.seq_organization_manglar_id')), true, 'FISCALIA', 'FISCALIA', 'admin');