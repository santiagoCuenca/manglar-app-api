
INSERT INTO manglar_app.organizations_manglar(
	organization_manglar_id, organization_manglar_status, organization_manglar_name, organization_manglar_complete_name, organization_manglar_type)
	VALUES ((SELECT nextval('manglar_app.seq_organization_manglar_id')), true, 'ConfigOrg', 'ConfigOrg', 'super-admin');

INSERT INTO manglar_app.allowed_users (
	allowed_user_id, allowed_user_status, organization_manglar_id, gelo_id, role_id, allowed_user_name, allowed_user_pin)
VALUES
	((SELECT nextval('manglar_app.seq_allowed_user_id')),
	true,
	(select organization_manglar_id from manglar_app.organizations_manglar where organization_manglar_name = 'ConfigOrg'),
	1,
	(select role_id from suia_iii.roles where role_name = 'ManglarApp-Administrador'),
	'Usuario super admin',
	'2222222222');
