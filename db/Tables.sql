
CREATE SCHEMA manglar_app
    AUTHORIZATION postgres;

CREATE TABLE manglar_app.anomalies_forms
(
    anomaly_form_id integer NOT NULL,
    user_id integer NOT NULL,
    anomaly_form_type character varying(255),
    anomaly_form_status boolean,
    anomaly_form_state character varying(255),
    created_at timestamp without time zone,
    area character varying(255),
    industrial_type character varying(255),
    artesanal_type character varying(255),
    fishing_type character varying(255),
    fishing_type_others character varying(255),
    date timestamp without time zone,
    beaching_reason character varying(255),
    animals_stranded_count integer,
    animals_stranded_size character varying(255),
    sea_conditions character varying(255),
    anomaly_form_subtype character varying(255),
    type character varying(255),
    custody character varying(255),
    protected_area character varying(255),
    description character varying(255),
    individuals_requisitioned_info character varying(255),
    individuals character varying(255),
    individuals_requisitioned character varying(255),
    province character varying(255),
    canton character varying(255),
    location character varying(255),
    latlong character varying(255),
    address character varying(255),
    estuary character varying(255),
    community character varying(255),
    feel_danger boolean,
    hide_identity boolean,
    CONSTRAINT pk_anomalies_forms PRIMARY KEY (anomaly_form_id)
);

ALTER TABLE ONLY manglar_app.anomalies_forms ADD CONSTRAINT fk_anomalies_forms FOREIGN KEY (user_id) REFERENCES public.users(user_id);

ALTER TABLE manglar_app.anomalies_forms
    OWNER to postgres;

CREATE INDEX anomaly_form_id ON manglar_app.anomalies_forms USING btree (anomaly_form_id);

CREATE SEQUENCE manglar_app.seq_anomaly_form_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE manglar_app.organizations_manglar
(
    organization_manglar_id integer NOT NULL,
    organization_manglar_status boolean,
    organization_manglar_name character varying(255),
    organization_manglar_complete_name character varying(255),
    organization_manglar_type character varying(255),
    CONSTRAINT pk_organizations_manglar PRIMARY KEY (organization_manglar_id)
);

ALTER TABLE manglar_app.organizations_manglar
    OWNER to postgres;

CREATE INDEX organization_manglar_id ON manglar_app.organizations_manglar USING btree (organization_manglar_id);

CREATE SEQUENCE manglar_app.seq_organization_manglar_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE manglar_app.organizations_users
(
    orus_id integer NOT NULL,
    orus_status boolean,
    organization_manglar_id integer NOT NULL,
    user_id integer NOT NULL,
    CONSTRAINT pk_organizations_users PRIMARY KEY (orus_id),
    CONSTRAINT fk_organization_manglar_id FOREIGN KEY (organization_manglar_id) REFERENCES manglar_app.organizations_manglar(organization_manglar_id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.users(user_id)
);

ALTER TABLE manglar_app.organizations_users
    OWNER to postgres;

CREATE INDEX orus_id ON manglar_app.organizations_users USING btree (orus_id);

CREATE SEQUENCE manglar_app.seq_orus_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE manglar_app.anomalies_evidences
(
    anomaly_evidence_id integer NOT NULL,
    anomaly_evidence_status boolean,
    anomaly_form_id integer NOT NULL,
    anomaly_evidence_name_file character varying(255),
    anomaly_evidence_type character varying(255),
    anomaly_evidence_path_origin character varying(255),
    anomaly_evidence_description character varying(255),
    anomaly_evidence_url character varying(255),
    CONSTRAINT pk_anomalies_evidences PRIMARY KEY (anomaly_evidence_id),
    CONSTRAINT user_id FOREIGN KEY (anomaly_form_id) REFERENCES manglar_app.anomalies_forms(anomaly_form_id)
);

ALTER TABLE manglar_app.anomalies_evidences
    OWNER to postgres;

CREATE INDEX anomaly_evidence_id ON manglar_app.anomalies_evidences USING btree (anomaly_evidence_id);

CREATE SEQUENCE manglar_app.seq_anomaly_evidence_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE manglar_app.anomalies_witnesses
(
    anomaly_witness_id integer NOT NULL,
    anomaly_witness_status boolean,
    anomaly_form_id integer NOT NULL,
  	anomaly_witness_name character varying(255),
  	anomaly_witness_phone character varying(255),
    CONSTRAINT pk_anomalies_witnesses PRIMARY KEY (anomaly_witness_id),
    CONSTRAINT user_id FOREIGN KEY (anomaly_form_id) REFERENCES manglar_app.anomalies_forms(anomaly_form_id)
);

ALTER TABLE manglar_app.anomalies_witnesses
    OWNER to postgres;

CREATE INDEX anomaly_witness_id ON manglar_app.anomalies_witnesses USING btree (anomaly_witness_id);

CREATE SEQUENCE manglar_app.seq_anomaly_witness_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE manglar_app.anomalies_offenders
(
    anomaly_offender_id integer NOT NULL,
    anomaly_offender_status boolean,
    anomaly_form_id integer NOT NULL,
	anomaly_offender_name character varying(255),
    anomaly_offender_pin character varying(255),
	anomaly_offender_phone character varying(255),
    anomaly_offender_address character varying(255),
    anomaly_offender_additional_information character varying(255),
    CONSTRAINT pk_anomalies_offenders PRIMARY KEY (anomaly_offender_id),
    CONSTRAINT user_id FOREIGN KEY (anomaly_form_id) REFERENCES manglar_app.anomalies_forms(anomaly_form_id)
);

ALTER TABLE manglar_app.anomalies_offenders
    OWNER to postgres;

CREATE INDEX anomaly_offender_id ON manglar_app.anomalies_offenders USING btree (anomaly_offender_id);

CREATE SEQUENCE manglar_app.seq_anomaly_offender_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;



CREATE TABLE manglar_app.email_notifications
(
    email_notification_id integer NOT NULL,
    email_notification_status boolean,
    email_notification_name character varying(255),
    email_notification_email character varying(255),
    email_notification_provinces character varying(355),
    email_notification_anomalies_types character varying(355),
    CONSTRAINT pk_email_notifications PRIMARY KEY (email_notification_id)
);

ALTER TABLE manglar_app.email_notifications
    OWNER to postgres;

CREATE INDEX email_notification_id ON manglar_app.email_notifications USING btree (email_notification_id);

CREATE SEQUENCE manglar_app.seq_email_notification_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


CREATE TABLE manglar_app.allowed_users
(
    allowed_user_id integer NOT NULL,
    allowed_user_status boolean,
    organization_manglar_id integer NOT NULL,
    gelo_id integer NOT NULL,
    role_id integer NOT NULL,
    allowed_user_name character varying(255),
    allowed_user_pin character varying(255),
    CONSTRAINT pk_allowed_users PRIMARY KEY (allowed_user_id),
    CONSTRAINT organization_manglar_id FOREIGN KEY (organization_manglar_id) REFERENCES manglar_app.organizations_manglar(organization_manglar_id),
    CONSTRAINT gelo_id FOREIGN KEY (gelo_id) REFERENCES public.geographical_locations(gelo_id),
    CONSTRAINT role_id FOREIGN KEY (role_id) REFERENCES suia_iii.roles(role_id)
);

ALTER TABLE manglar_app.allowed_users
    OWNER to postgres;

CREATE INDEX allowed_user_id ON manglar_app.allowed_users USING btree (allowed_user_id);

CREATE SEQUENCE manglar_app.seq_allowed_user_id
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;
