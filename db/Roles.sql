

INSERT INTO suia_iii.roles(
    role_id, role_description, role_name, role_unique, role_status, role_sistemas, role_used_authority, role_allow_administer)
    VALUES 
    ((SELECT nextval('suia_iii.seq_role_id')), 'ManglarApp-Socio', 'ManglarApp-Socio', true, true, 'ManglarApp-Socio', 'ManglarApp-Socio', true),
    ((SELECT nextval('suia_iii.seq_role_id')), 'ManglarApp-Tecnico', 'ManglarApp-Tecnico', true, true, 'ManglarApp-Tecnico', 'ManglarApp-Tecnico', true),
    ((SELECT nextval('suia_iii.seq_role_id')), 'ManglarApp-Administrador', 'ManglarApp-Administrador', true, true, 'ManglarApp-Administrador', 'ManglarApp-Administrador', true)
    ;
