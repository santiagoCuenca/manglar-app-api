package ec.gob.ambiente.manglarapp.services;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.gob.ambiente.enlisy.dao.AbstractFacade;
import ec.gob.ambiente.manglarapp.model.OrganizationManglar;
import ec.gob.ambiente.manglarapp.model.OrganizationManglar;

@Stateless
public class OrganizationManglarFacade extends AbstractFacade<OrganizationManglar, Integer> {

	public OrganizationManglarFacade() {
		super(OrganizationManglar.class, Integer.class);		
	}
	
	public boolean save(OrganizationManglar organizationManglar)
	{
		try
		{						
			if (organizationManglar.getOrganizationManglarId() == null) {
				create(organizationManglar);
			} else {
				edit(organizationManglar);
			}
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	/**
	 * Buscar por Id
	 * @param organizationManglarId
	 * @return
	 */
	public OrganizationManglar findById(Integer organizationManglarId)
	{
		Query query = super.getEntityManager().createQuery("select o from OrganizationManglar o where o.organizationManglarId = :organizationManglarId and o.organizationManglarStatus = true");
		query.setParameter("organizationManglarId", organizationManglarId);
		
		OrganizationManglar organizationManglar = null;
		
		if(query.getResultList().size() > 0)
		{
			organizationManglar = (OrganizationManglar) query.getResultList().get(0);
		}
		
		return organizationManglar;
	}

}
