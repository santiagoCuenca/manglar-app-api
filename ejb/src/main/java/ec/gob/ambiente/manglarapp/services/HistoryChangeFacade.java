package ec.gob.ambiente.manglarapp.services;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.gob.ambiente.enlisy.dao.AbstractFacade;
import ec.gob.ambiente.manglarapp.model.HistoryChange;

@Stateless
public class HistoryChangeFacade extends AbstractFacade<HistoryChange, Integer> {

	public HistoryChangeFacade() {
		super(HistoryChange.class, Integer.class);		
	}
	
	public boolean save(HistoryChange form)
	{
		try
		{						
			if (form.getId() == null) {
				form.setDate(new Date());
				form.setHistoryChangeStatus(true);
				create(form);
			} else {
				edit(form);
			}
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<HistoryChange> findByForm(Integer formId)
	{
		Query query = super.getEntityManager().createQuery("select a from HistoryChange a where a.formId = :formId and a.historyChangeStatus = true");
		query.setParameter("formId", formId);		
		return (List<HistoryChange>) query.getResultList();
	}

}
