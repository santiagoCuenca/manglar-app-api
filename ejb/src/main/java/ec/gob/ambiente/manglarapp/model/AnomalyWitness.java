package ec.gob.ambiente.manglarapp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ec.gob.ambiente.enlisy.model.Organization;
import ec.gob.ambiente.enlisy.model.User;

@Entity
@Table(name="anomalies_witnesses", schema="manglar_app")
@NamedQuery(name="AnomalyWitness.findAll", query="SELECT o FROM AnomalyWitness o")
public class AnomalyWitness implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ANOMALIES_WITNESSES_GENERATOR", initialValue = 1, sequenceName = "seq_anomaly_witness_id", schema = "manglar_app", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANOMALIES_WITNESSES_GENERATOR")
	@Getter
	@Setter
	@Column(name="anomaly_witness_id")
	private Integer anomalyWitnessId;

	@Getter
	@Setter
	@Column(name="anomaly_witness_status")
	private Boolean anomalyWitnessStatus;

	// NOT USE GETTER SINCE PRODUCES INFINITE RESPONSE
	@Setter
	@ManyToOne
	@JoinColumn(name="anomaly_form_id")
	private AnomalyForm anomalyForm;

	@Getter
	@Setter
	@Column(name="anomaly_witness_name")
	private String anomalyWitnessName;

	@Getter
	@Setter
	@Column(name="anomaly_witness_phone")
	private String anomalyWitnessPhone;

}
