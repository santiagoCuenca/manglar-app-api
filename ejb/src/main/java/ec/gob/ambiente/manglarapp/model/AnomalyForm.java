package ec.gob.ambiente.manglarapp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "anomalies_forms", schema = "manglar_app")
@NamedQuery(name = "AnomalyForm.findAll", query = "SELECT o FROM AnomalyForm o")
public class AnomalyForm implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ANOMALIES_FORMS_GENERATOR", initialValue = 1, sequenceName = "seq_anomaly_form_id", schema = "manglar_app", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANOMALIES_FORMS_GENERATOR")
	@Getter
	@Setter
	@Column(name = "anomaly_form_id")
	private Integer anomalyFormId;

	@Getter
	@Setter
	@Column(name = "user_id")
	private Integer userId;

	// bi-directional many-to-one association to Contact
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "anomalyForm")
	@LazyCollection(LazyCollectionOption.FALSE)
	@Getter
	@Setter
	@JsonProperty
	private List<AnomalyOffender> anomaliesOffenders;

	// bi-directional many-to-one association to Contact
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "anomalyForm")
	@LazyCollection(LazyCollectionOption.FALSE)
	@Getter
	@Setter
	@JsonProperty
	private List<AnomalyWitness> anomaliesWitnesses;

	// bi-directional many-to-one association to Contact
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "anomalyForm")
	@LazyCollection(LazyCollectionOption.FALSE)
	@Getter
	@Setter
	@JsonProperty
	private List<AnomalyEvidence> anomaliesEvidences;

	@Getter
	@Setter
	@Column(name = "anomaly_form_type")
	private String anomalyFormType;

	@Getter
	@Setter
	@Column(name = "anomaly_form_status")
	private Boolean anomalyFormStatus;

	@Getter
	@Setter
	@Column(name = "anomaly_form_state")
	private String anomalyFormState;

	@Getter
	@Setter
	@Column(name = "created_at")
	private Date createdAt;

	@Getter
	@Setter
	@Column(name = "area")
	private String area;

	@Getter
	@Setter
	@Column(name = "industrial_type")
	private String industrialType;

	@Getter
	@Setter
	@Column(name = "artesanal_type")
	private String artesanalType;

	@Getter
	@Setter
	@Column(name = "fishing_type")
	private String fishingType;

	@Getter
	@Setter
	@Column(name = "fishing_type_others")
	private String fishingTypeOthers;

	@Getter
	@Setter
	@Column(name = "date")
	private Date date;

	@Getter
	@Setter
	@Column(name = "beaching_reason")
	private String beachingReason;

	@Getter
	@Setter
	@Column(name = "animals_stranded_count")
	private Integer animalsStrandedCount;

	@Getter
	@Setter
	@Column(name = "animals_stranded_size")
	private String animalsStrandedSize;

	@Getter
	@Setter
	@Column(name = "sea_conditions")
	private String seaConditions;

	@Getter
	@Setter
	@Column(name = "anomaly_form_subtype")
	private String anomalyFormSubtype;

	@Getter
	@Setter
	@Column(name = "type")
	private String type;

	@Getter
	@Setter
	@Column(name = "custody")
	private String custody;

	@Getter
	@Setter
	@Column(name = "protected_area")
	private String protectedArea;

	@Getter
	@Setter
	@Column(name = "description")
	private String description;

	@Getter
	@Setter
	@Column(name = "individuals_requisitioned_info")
	private String individualsRequisitionedInfo;

	@Getter
	@Setter
	@Column(name = "individuals")
	private String individuals;

	@Getter
	@Setter
	@Column(name = "individuals_requisitioned")
	private String individualsRequisitioned;

	@Getter
	@Setter
	@Column(name = "province")
	private String province;

	@Getter
	@Setter
	@Column(name = "canton")
	private String canton;

	@Getter
	@Setter
	@Column(name = "location")
	private String location;

	@Getter
	@Setter
	@Column(name = "latlong")
	private String latlong;

	@Getter
	@Setter
	@Column(name = "address")
	private String address;

	@Getter
	@Setter
	@Column(name = "estuary")
	private String estuary;

	@Getter
	@Setter
	@Column(name = "community")
	private String community;

	@Getter
	@Setter
	@Column(name = "feel_danger")
	private Boolean feelDanger;

	@Getter
	@Setter
	@Column(name = "hide_identity")
	private Boolean hideIdentity;

}
