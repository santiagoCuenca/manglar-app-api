package ec.gob.ambiente.manglarapp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ec.gob.ambiente.enlisy.model.Organization;
import ec.gob.ambiente.enlisy.model.User;

@Entity
@Table(name="anomalies_evidences", schema="manglar_app")
@NamedQuery(name="AnomalyEvidence.findAll", query="SELECT o FROM AnomalyEvidence o")
public class AnomalyEvidence implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ANOMALIES_EVIDENCES_GENERATOR", initialValue = 1, sequenceName = "seq_anomaly_evidence_id", schema = "manglar_app", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANOMALIES_EVIDENCES_GENERATOR")
	@Getter
	@Setter
	@Column(name="anomaly_evidence_id")
	private Integer anomalyEvidenceId;

	@Getter
	@Setter
	@Column(name="anomaly_evidence_status")
	private Boolean anomalyEvidenceStatus;

	// NOT USE GETTER SINCE PRODUCES INFINITE RESPONSE
	@Setter
	@ManyToOne
	@JoinColumn(name="anomaly_form_id")
	private AnomalyForm anomalyForm;

	@Getter
	@Setter
	@Column(name="anomaly_evidence_name_file")
	private String anomalyEvidenceNameFile;

	@Getter
	@Setter
	@Column(name="anomaly_evidence_type")
	private String anomalyEvidenceType;

	@Getter
	@Setter
	@Column(name="anomaly_evidence_path_origin")
	private String anomalyEvidencePathOrigin;

	@Getter
	@Setter
	@Column(name="anomaly_evidence_description")
	private String anomalyEvidenceDescription;

	@Getter
	@Setter
	@Column(name="anomaly_evidence_url")
	private String anomalyEvidenceUrl;

}
