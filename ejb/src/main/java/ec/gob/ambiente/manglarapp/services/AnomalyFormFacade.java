package ec.gob.ambiente.manglarapp.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.gob.ambiente.enlisy.dao.AbstractFacade;
import ec.gob.ambiente.manglarapp.model.AnomalyForm;

@Stateless
public class AnomalyFormFacade extends AbstractFacade<AnomalyForm, Integer> {

	public AnomalyFormFacade() {
		super(AnomalyForm.class, Integer.class);		
	}
	
	public boolean save(AnomalyForm anomalyForm)
	{
		try
		{						
			if (anomalyForm.getAnomalyFormId() == null) {
				anomalyForm.setCreatedAt(new Date());
				create(anomalyForm);
			} else {
				edit(anomalyForm);
			}
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	/**
	 * Buscar por userId
	 * @param organizationManglarId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<AnomalyForm> getByUserId(Integer userId) {
		
		Query query = super.getEntityManager().createQuery("select o from AnomalyForm o where o.userId = :userId");
		query.setParameter("userId", userId);
		
		List<AnomalyForm> result = new ArrayList<>();
		
		if(query.getResultList().size() > 0)
		{
			result = (List<AnomalyForm>) query.getResultList();
		}
		
		return result;
	}

	/**
	 * Buscar por type
	 * @param organizationManglarId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<AnomalyForm> findByType(String type) {
		
		Query query = super.getEntityManager().createQuery("select o from AnomalyForm o where o.anomalyFormType = :type");
		query.setParameter("type", type);
		
		List<AnomalyForm> result = new ArrayList<>();
		
		if(query.getResultList().size() > 0)
		{
			result = (List<AnomalyForm>) query.getResultList();
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<AnomalyForm> findByTypeProvince(String type, String province) {
		Query query = super.getEntityManager().createQuery("select o from AnomalyForm o where o.anomalyFormType = :type and o.province = :province");
		query.setParameter("type", type);
		query.setParameter("province", province);
		List<AnomalyForm> result = new ArrayList<>();
		if(query.getResultList().size() > 0)
		{
			result = (List<AnomalyForm>) query.getResultList();
		}
		return result;
	}
	
	/**
	 * group by type count
	 * @param organizationManglarId
	 * @return
	 */
	public List<?> summary() {
		Query query = super.getEntityManager().createQuery("select o.anomalyFormType, count(*) from AnomalyForm o group by o.anomalyFormType");
		return query.getResultList();
	}

	public List<?> summaryByTypeAndProvince(List<String> types, String province) {
		String typeConditions = types.size() == 0 ? "" : getTypeCondition(types);
		Query query = super.getEntityManager().createQuery("select o.anomalyFormType, count(*) from AnomalyForm o where o.province = :province " + typeConditions + " group by o.anomalyFormType");
		query.setParameter("province", province);
		return query.getResultList();
	}

	private String getTypeCondition(List<String> types){
		List<String> typeConditions = new ArrayList<>();
		for (String type : types) {
			typeConditions.add("o.anomalyFormType = '" + type + "'");
		}
		return "and (" + strJoin(typeConditions.toArray(new String[0]), " or ") + ")";
	}

	private String strJoin(String[] aArr, String sSep) {
	    StringBuilder sbStr = new StringBuilder();
	    for (int i = 0, il = aArr.length; i < il; i++) {
	        if (i > 0)
	            sbStr.append(sSep);
	        sbStr.append(aArr[i]);
	    }
	    return sbStr.toString();
	}

}
