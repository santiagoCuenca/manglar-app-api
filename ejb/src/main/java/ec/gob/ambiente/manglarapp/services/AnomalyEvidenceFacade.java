package ec.gob.ambiente.manglarapp.services;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.gob.ambiente.enlisy.dao.AbstractFacade;
import ec.gob.ambiente.manglarapp.model.AnomalyEvidence;

@Stateless
public class AnomalyEvidenceFacade extends AbstractFacade<AnomalyEvidence, Integer> {

	public AnomalyEvidenceFacade() {
		super(AnomalyEvidence.class, Integer.class);		
	}
	
	public boolean save(AnomalyEvidence anomalyEvidence)
	{
		try
		{						
			if (anomalyEvidence.getAnomalyEvidenceId() == null) {
				create(anomalyEvidence);
			} else {
				edit(anomalyEvidence);
			}
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	/**
	 * Buscar por Id
	 * @param anomalyEvidenceId
	 * @return
	 */
	public AnomalyEvidence findById(Integer anomalyEvidenceId)
	{
		Query query = super.getEntityManager().createQuery("select o from AnomalyEvidence o where o.anomalyEvidenceId = :anomalyEvidenceId and o.anomalyEvidenceStatus = true");
		query.setParameter("anomalyEvidenceId", anomalyEvidenceId);
		
		AnomalyEvidence anomalyEvidence = null;
		
		if(query.getResultList().size() > 0)
		{
			anomalyEvidence = (AnomalyEvidence) query.getResultList().get(0);
		}
		
		return anomalyEvidence;
	}

	/**
	 * Buscar por Pin
	 * @param anomalyEvidencePin
	 * @return
	 */
	public AnomalyEvidence findByUserPin(String anomalyEvidencePin)
	{
		Query query = super.getEntityManager().createQuery("select o from AnomalyEvidence o where o.anomalyEvidencePin = :anomalyEvidencePin and o.anomalyEvidenceStatus = true");
		query.setParameter("anomalyEvidencePin", anomalyEvidencePin);
		
		AnomalyEvidence anomalyEvidence = null;
		
		if(query.getResultList().size() > 0)
		{
			anomalyEvidence = (AnomalyEvidence) query.getResultList().get(0);
		}
		
		return anomalyEvidence;
	}

}
