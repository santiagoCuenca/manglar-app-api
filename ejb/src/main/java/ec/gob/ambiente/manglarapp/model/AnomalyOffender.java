package ec.gob.ambiente.manglarapp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ec.gob.ambiente.enlisy.model.Organization;
import ec.gob.ambiente.enlisy.model.User;

@Entity
@Table(name="anomalies_offenders", schema="manglar_app")
@NamedQuery(name="AnomalyOffender.findAll", query="SELECT o FROM AnomalyOffender o")
public class AnomalyOffender implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ANOMALIES_OFFENDERS_GENERATOR", initialValue = 1, sequenceName = "seq_anomaly_offender_id", schema = "manglar_app", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ANOMALIES_OFFENDERS_GENERATOR")
	@Getter
	@Setter
	@Column(name="anomaly_offender_id")
	private Integer anomalyOffenderId;

	@Getter
	@Setter
	@Column(name="anomaly_offender_status")
	private Boolean anomalyOffenderStatus;

	// NOT USE GETTER SINCE PRODUCES INFINITE RESPONSE
	@Setter
	@ManyToOne
	@JoinColumn(name="anomaly_form_id")
	private AnomalyForm anomalyForm;

	@Getter
	@Setter
	@Column(name="anomaly_offender_name")
	private String anomalyOffenderName;

	@Getter
	@Setter
	@Column(name="anomaly_offender_pin")
	private String anomalyOffenderPin;

	@Getter
	@Setter
	@Column(name="anomaly_offender_phone")
	private String anomalyOffenderPhone;

	@Getter
	@Setter
	@Column(name="anomaly_offender_address")
	private String anomalyOffenderAddress;

	@Getter
	@Setter
	@Column(name="anomaly_offender_additional_information")
	private String anomalyOffenderAdditionalInformation;

}
