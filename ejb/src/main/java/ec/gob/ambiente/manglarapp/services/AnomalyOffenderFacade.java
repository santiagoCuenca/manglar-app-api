package ec.gob.ambiente.manglarapp.services;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.gob.ambiente.enlisy.dao.AbstractFacade;
import ec.gob.ambiente.manglarapp.model.AnomalyOffender;

@Stateless
public class AnomalyOffenderFacade extends AbstractFacade<AnomalyOffender, Integer> {

	public AnomalyOffenderFacade() {
		super(AnomalyOffender.class, Integer.class);		
	}
	
	public boolean save(AnomalyOffender anomalyOffender)
	{
		try
		{						
			if (anomalyOffender.getAnomalyOffenderId() == null) {
				create(anomalyOffender);
			} else {
				edit(anomalyOffender);
			}
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	/**
	 * Buscar por Id
	 * @param anomalyOffenderId
	 * @return
	 */
	public AnomalyOffender findById(Integer anomalyOffenderId)
	{
		Query query = super.getEntityManager().createQuery("select o from AnomalyOffender o where o.anomalyOffenderId = :anomalyOffenderId and o.anomalyOffenderStatus = true");
		query.setParameter("anomalyOffenderId", anomalyOffenderId);
		
		AnomalyOffender anomalyOffender = null;
		
		if(query.getResultList().size() > 0)
		{
			anomalyOffender = (AnomalyOffender) query.getResultList().get(0);
		}
		
		return anomalyOffender;
	}

	/**
	 * Buscar por Pin
	 * @param anomalyOffenderPin
	 * @return
	 */
	public AnomalyOffender findByUserPin(String anomalyOffenderPin)
	{
		Query query = super.getEntityManager().createQuery("select o from AnomalyOffender o where o.anomalyOffenderPin = :anomalyOffenderPin and o.anomalyOffenderStatus = true");
		query.setParameter("anomalyOffenderPin", anomalyOffenderPin);
		
		AnomalyOffender anomalyOffender = null;
		
		if(query.getResultList().size() > 0)
		{
			anomalyOffender = (AnomalyOffender) query.getResultList().get(0);
		}
		
		return anomalyOffender;
	}

}
