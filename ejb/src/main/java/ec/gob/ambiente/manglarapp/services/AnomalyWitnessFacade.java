package ec.gob.ambiente.manglarapp.services;

import javax.ejb.Stateless;
import javax.persistence.Query;

import ec.gob.ambiente.enlisy.dao.AbstractFacade;
import ec.gob.ambiente.manglarapp.model.AnomalyWitness;

@Stateless
public class AnomalyWitnessFacade extends AbstractFacade<AnomalyWitness, Integer> {

	public AnomalyWitnessFacade() {
		super(AnomalyWitness.class, Integer.class);		
	}
	
	public boolean save(AnomalyWitness anomalyWitness)
	{
		try
		{						
			if (anomalyWitness.getAnomalyWitnessId() == null) {
				create(anomalyWitness);
			} else {
				edit(anomalyWitness);
			}
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}

	/**
	 * Buscar por Id
	 * @param anomalyWitnessId
	 * @return
	 */
	public AnomalyWitness findById(Integer anomalyWitnessId)
	{
		Query query = super.getEntityManager().createQuery("select o from AnomalyWitness o where o.anomalyWitnessId = :anomalyWitnessId and o.anomalyWitnessStatus = true");
		query.setParameter("anomalyWitnessId", anomalyWitnessId);
		
		AnomalyWitness anomalyWitness = null;
		
		if(query.getResultList().size() > 0)
		{
			anomalyWitness = (AnomalyWitness) query.getResultList().get(0);
		}
		
		return anomalyWitness;
	}

	/**
	 * Buscar por Pin
	 * @param anomalyWitnessPin
	 * @return
	 */
	public AnomalyWitness findByUserPin(String anomalyWitnessPin)
	{
		Query query = super.getEntityManager().createQuery("select o from AnomalyWitness o where o.anomalyWitnessPin = :anomalyWitnessPin and o.anomalyWitnessStatus = true");
		query.setParameter("anomalyWitnessPin", anomalyWitnessPin);
		
		AnomalyWitness anomalyWitness = null;
		
		if(query.getResultList().size() > 0)
		{
			anomalyWitness = (AnomalyWitness) query.getResultList().get(0);
		}
		
		return anomalyWitness;
	}

}
