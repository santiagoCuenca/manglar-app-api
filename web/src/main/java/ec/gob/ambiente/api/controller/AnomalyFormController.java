package ec.gob.ambiente.api.controller;

import java.util.List;

import ec.gob.ambiente.api.controller.AnomalyFormController;
import ec.gob.ambiente.enlisy.model.User;
import ec.gob.ambiente.enlisy.services.UserFacade;
import ec.gob.ambiente.manglarapp.model.AnomalyEvidence;
import ec.gob.ambiente.manglarapp.model.AnomalyForm;
import ec.gob.ambiente.manglarapp.model.AnomalyOffender;
import ec.gob.ambiente.manglarapp.model.AnomalyWitness;
import ec.gob.ambiente.manglarapp.model.EmailNotification;
import ec.gob.ambiente.manglarapp.services.AnomalyFormFacade;
import ec.gob.ambiente.manglarapp.services.EmailNotificationFacade;
import ec.gob.ambiente.util.Email;
import lombok.Getter;
import lombok.Setter;

public class AnomalyFormController {

	@Getter
    @Setter
    private String msg;
	
	private UserFacade userFacade;
	private AnomalyFormFacade anomalyFormFacade;
	private EmailNotificationFacade emailNotificationFacade;

	public AnomalyFormController(UserFacade userFacade,
			AnomalyFormFacade anomalyFormFacade,
			EmailNotificationFacade emailNotificationFacade) {
		super();
		this.userFacade = userFacade;
		this.anomalyFormFacade = anomalyFormFacade;
		this.emailNotificationFacade = emailNotificationFacade;
	}

	public static String getTypeName(String typeId){
		switch(typeId) {
		case "contaminacion":
			return "Contaminación";

		case "epoca_veda":
			return "Recolección en epoca de veda";

		case "invasiones":
			return "Invasiones";

		case "delincuencia_maritima":
			return "Delincuencia marítima";

		case "tala":
			return "Tala";

		case "tallas_minimas":
			return "Incumplimento de talla mínimas";

		case "tecnicas_pesca":
			return "Técnicas de pesca no permitidas";

		case "trafico_especies":
			return "Tráfico y pesca de especies amenazadas";

		case "vida_silvestre":
			return "Hallazgos de vida silvestre";
		}
		return "NO_TYPE";
	};

	public boolean save(AnomalyForm anomalyForm, String urlBase) {
		
		setAnomalyFormRelations(anomalyForm);

		boolean successSave = anomalyFormFacade.save(anomalyForm);
		
		if (!successSave) {
			msg = "No se guardo el formulario";
			return false;
		}
		String province = anomalyForm.getProvince();
		String anomalyType = anomalyForm.getAnomalyFormType();
		User userCreateReport = (User) userFacade.find(anomalyForm.getUserId());
		String userNameCreateReport = userCreateReport.getPeople().getPeopName();
		String userEmailCreateReport = userCreateReport.getPeople().getEmail();

		String urlPdf = urlBase + "rest/pdf/form/" + anomalyForm.getAnomalyFormId();

		// SEND EMAIL USER
		sendEmailUserNewForm(userEmailCreateReport, userNameCreateReport, anomalyType, urlPdf);
		
		// SEND EMAILS ADMINS
		List<EmailNotification> emailNotifications = emailNotificationFacade
				.findByProvinceAndAnomalyForm(province, anomalyType);
		if (emailNotifications.size() == 0 ) {
			System.out.println("El formulario se guardó, pero no se encontró configuración de correos para: " + province + " " + anomalyType);
		}
		sendEmailsAdminsNewForm(emailNotifications, userNameCreateReport, anomalyType, urlPdf);
		return true;
	}

	public boolean updateState(Integer id, String state, String urlBase){
		AnomalyForm anomalyForm = (AnomalyForm) anomalyFormFacade.find(id);
		anomalyForm.setAnomalyFormState(state);
		boolean success = anomalyFormFacade.save(anomalyForm);
		if (!success) {
			msg = "No se actualizó correctamente el form " + id;
			return false;
		}
		String province = anomalyForm.getProvince();
		String anomalyType = anomalyForm.getAnomalyFormType();
		User userCreateReport = (User) userFacade.find(anomalyForm.getUserId());
		String userNameCreateReport = userCreateReport.getPeople().getPeopName();
		String userEmailCreateReport = userCreateReport.getPeople().getEmail();

		String urlPdf = urlBase + "rest/pdf/form/" + id;

		// SEND EMAIL USER
		sendEmailUserUpdateForm(userEmailCreateReport, userNameCreateReport, anomalyType, id.toString(), state, urlPdf);

		// SEND EMAILS ADMINS
		List<EmailNotification> emailNotifications = emailNotificationFacade
				.findByProvinceAndAnomalyForm(province, anomalyType);
		if (emailNotifications.size() == 0 ) {
			System.out.println("El formulario se guardó, pero no se encontró configuración de correos para: " + province + " " + anomalyType);
		}
		sendEmailsAdminsUpdate(emailNotifications, userNameCreateReport, anomalyType, id.toString(), state, urlPdf);

		return true;
	}

	// Set AnomalyForm since relations not populate the data before save
	private void setAnomalyFormRelations(AnomalyForm anomalyForm){
		for( AnomalyOffender anomalyOffender : anomalyForm.getAnomaliesOffenders()) {
			anomalyOffender.setAnomalyForm(anomalyForm);
		}
		for( AnomalyWitness anomalyWitness : anomalyForm.getAnomaliesWitnesses()) {
			anomalyWitness.setAnomalyForm(anomalyForm);
		}
		for( AnomalyEvidence anomalyEvidence : anomalyForm.getAnomaliesEvidences()) {
			anomalyEvidence.setAnomalyForm(anomalyForm);
		}
		
	}

	private void sendEmailUserNewForm(String emailDestino, String userNameCreateReport, String anomalyType, String urlPdf) {
		String asunto = "ManglarApp - Nueva anomalía reportada";
		String mensaje = getAsuntoUser(userNameCreateReport, anomalyType, urlPdf);
		if ( emailDestino == null ) {
			System.out.println("No se encontro correo para el usuario " + userNameCreateReport);
		}
		else {
	    	Email.sendEmail(emailDestino, asunto, mensaje);
		}
	}

	private void sendEmailUserUpdateForm(String emailDestino, String userNameCreateReport, String anomalyType, String id, String state, String urlPdf) {
		String asunto = "ManglarApp - Formulario actualizado";
		String mensaje = getAsuntoUserUpdate(userNameCreateReport, anomalyType, id, state, urlPdf);
		if ( emailDestino == null ) {
			System.out.println("No se encontro correo para el usuario " + userNameCreateReport);
		}
		else {
	    	Email.sendEmail(emailDestino, asunto, mensaje);
		}
	}

	private void sendEmailsAdminsNewForm(List<EmailNotification> emailNotifications, String userNameCreateReport, String anomalyType, String urlPdf) {
		String asunto = "ManglarApp - Nueva anomalía reportada";
		for ( EmailNotification emailNotification : emailNotifications ) {
			String userNameControl = emailNotification.getEmailNotificationName();
			String mensaje = getAsuntoAdmins(userNameControl, userNameCreateReport, anomalyType, urlPdf);
			String emailDestino = emailNotification.getEmailNotificationEmail();
			System.out.println("Se envia el correo " + userNameControl + " " + emailDestino + " " + mensaje);
			if ( emailDestino == null ) {
				System.out.println("No se encontro correo para el usuario " + userNameControl + " " + emailNotification.getEmailNotificationId());
			}
			else {
		    	Email.sendEmail(emailDestino, asunto, mensaje);
			}
		}
	}

	private void sendEmailsAdminsUpdate(List<EmailNotification> emailNotifications, String userNameCreateReport, String anomalyType, String id, String state, String urlPdf) {
		String asunto = "ManglarApp - Formulario actualizado";
		for ( EmailNotification emailNotification : emailNotifications ) {
			String userNameControl = emailNotification.getEmailNotificationName();
			String mensaje = getAsuntoAdminsUpdate(userNameControl, userNameCreateReport, anomalyType, id, state, urlPdf);
			String emailDestino = emailNotification.getEmailNotificationEmail();
			if ( emailDestino == null ) {
				System.out.println("No se encontro correo para el usuario " + userNameControl + " " + emailNotification.getEmailNotificationId());
			}
			else {
		    	Email.sendEmail(emailDestino, asunto, mensaje);
			}
		}
	}

	private String getAsuntoUser(String userName, String anomalyType, String urlPdf){
		StringBuilder mensaje = new StringBuilder();
			mensaje.append("<p>Estimado/a Se&ntilde;or/a ").append(userName)
					.append("</p>").append("<br/>");
			mensaje.append("<p>Le informamos que ha reportado una nueva anomalía de tipo ").append(AnomalyFormController.getTypeName(anomalyType)).append("</p>");
			mensaje.append("<p>" + urlPdf + "</p><br/>");

			mensaje.append(
					"<p>Agradecemos su denuncia, pronto daremos respuesta a su reporte")
					.append("</p><br/>");
			mensaje.append("<p>Saludos").append("</p>");
			mensaje.append("<p>Ministerio del Ambiente").append("</p>")
					.append("<br/>");
		return mensaje.toString();
	}

	private String getAsuntoUserUpdate(String userName, String anomalyType, String id, String state, String urlPdf){
		StringBuilder mensaje = new StringBuilder();
			mensaje.append("<p>Estimado/a Se&ntilde;or/a ").append(userName)
					.append("</p>").append("<br/>");
			mensaje.append("<p>Le informamos que su reporte con id: ").append(id).append(" de tipo: ")
			.append(AnomalyFormController.getTypeName(anomalyType)).append(" cambió de estado a: ").append(state).append("</p>");
			mensaje.append("<p>" + urlPdf + "</p><br/>");
			mensaje.append("<p>Para revisar este informe, puede acceder a nuestra aplicación de usuarios")
					.append("</p><br/>");
			mensaje.append("<p>Saludos").append("</p>");
			mensaje.append("<p>Ministerio del Ambiente").append("</p>")
					.append("<br/>");
		return mensaje.toString();
	}

	private String getAsuntoAdminsUpdate(String userNameControl, String userNameCreateReport, String anomalyType, String id, String state, String urlPdf){
		StringBuilder mensaje = new StringBuilder();
			mensaje.append("<p>Estimado/a Se&ntilde;or/a ").append(userNameControl)
					.append("</p>").append("<br/>");
			mensaje.append("<p>Le informamos que se actualizó correctamente el formulario con id: ").append(id)
				.append(" reportado por: ").append(userNameCreateReport).append(" de tipo: ").append(AnomalyFormController.getTypeName(anomalyType))
				.append(", nuevo estado: ").append(state).append("</p>");
			mensaje.append("<p>" + urlPdf + "</p><br/>");
			mensaje.append("<p>Para revisar este informe, puede acceder a nuestra aplicación de administradores")
					.append("</p><br/>");
			mensaje.append("<p>Saludos").append("</p>");
			mensaje.append("<p>Ministerio del Ambiente").append("</p>")
					.append("<br/>");
		return mensaje.toString();
	}

	private String getAsuntoAdmins(String userNameControl, String userNameCreateReport, String anomalyType, String urlPdf){
		StringBuilder mensaje = new StringBuilder();
			mensaje.append("<p>Estimado/a Se&ntilde;or/a ").append(userNameControl)
					.append("</p>").append("<br/>");
			mensaje.append("<p>Le informamos que el usuario ").append(userNameCreateReport);
			mensaje.append(" ha reportado una nueva anomalía de tipo ").append(AnomalyFormController.getTypeName(anomalyType)).append("</p>");
			mensaje.append("<p>" + urlPdf + "</p><br/>");
			mensaje.append("<p>Para revisar este informe, puede acceder a nuestra aplicación de administradores")
					.append("</p><br/>");
			mensaje.append("<p>Saludos").append("</p>");
			mensaje.append("<p>Ministerio del Ambiente").append("</p>")
					.append("<br/>");
		return mensaje.toString();
	}

}
