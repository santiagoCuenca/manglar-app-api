package ec.gob.ambiente.api.resource;

import java.util.Arrays;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import lombok.Getter;
import lombok.Setter;
import ec.gob.ambiente.api.controller.AnomalyFormController;
import ec.gob.ambiente.api.history.HistoryChangeUtil;
import ec.gob.ambiente.api.model.DataResponse;
import ec.gob.ambiente.enlisy.model.User;
import ec.gob.ambiente.enlisy.services.UserFacade;
import ec.gob.ambiente.manglarapp.model.AllowedUser;
import ec.gob.ambiente.manglarapp.model.AnomalyEvidence;
import ec.gob.ambiente.manglarapp.model.AnomalyForm;
import ec.gob.ambiente.manglarapp.model.AnomalyOffender;
import ec.gob.ambiente.manglarapp.model.AnomalyWitness;
import ec.gob.ambiente.manglarapp.model.OrganizationManglar;
import ec.gob.ambiente.manglarapp.services.AllowedUserFacade;
import ec.gob.ambiente.manglarapp.services.AnomalyFormFacade;
import ec.gob.ambiente.manglarapp.services.EmailNotificationFacade;
import ec.gob.ambiente.manglarapp.services.HistoryChangeFacade;
import ec.gob.ambiente.manglarapp.services.OrganizationsUserFacade;

@Path("/")
public class AnomalyFormResource {

	@EJB
	private UserFacade userFacade;

	@EJB
	private AnomalyFormFacade anomalyFormFacade;

	@EJB
	private EmailNotificationFacade emailNotificationFacade;

	@EJB
	private OrganizationsUserFacade organizationsUserFacade;

	@EJB
	private HistoryChangeFacade historyChangeFacade;

	@EJB
	private AllowedUserFacade allowedUserFacade;

	@POST
	@Path("/anomaly-form-reporter/save")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataResponse save(@Context HttpServletRequest servletRequest, AnomalyForm anomalyForm) {
		AnomalyFormController anomalyFormController = new AnomalyFormController(
				userFacade, anomalyFormFacade, emailNotificationFacade);
		String urlBase = servletRequest.getHeader("ServerUrl");
		boolean success = anomalyFormController.save(anomalyForm, urlBase);
		if (success) {
			return new DataResponse(DataResponse.SUCCESS_STATE);
		}
		return new DataResponse(DataResponse.ERROR_STATE + " " + anomalyFormController.getMsg());
	}

	@GET
	@Path("/anomaly-form-reporter/get/{user-id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AnomalyFormDetail> getAnomaliesForm(@PathParam("user-id") Integer userId) {
		List<AnomalyForm> anomaliesForms = anomalyFormFacade.getByUserId(userId);
		List<AnomalyFormDetail> anomaliesFormDetail = new ArrayList<>();
		for(AnomalyForm anomalyForm : anomaliesForms) {
			anomaliesFormDetail.add(new AnomalyFormDetail(anomalyForm, userFacade, organizationsUserFacade));
		}
		return anomaliesFormDetail;
	}

	@POST
	@Path("/anomaly-form/update-state")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataResponse updateState(@Context HttpServletRequest servletRequest, UpdateAnomalyForm updateAnomalyForm) {
		AnomalyFormController anomalyFormController = new AnomalyFormController(
				userFacade, anomalyFormFacade, emailNotificationFacade);
		AnomalyForm current = anomalyFormFacade.find(updateAnomalyForm.getId());
		String oldState = current.getAnomalyFormState();
		String urlBase = servletRequest.getHeader("ServerUrl");
		boolean success = anomalyFormController.updateState(updateAnomalyForm.getId(), updateAnomalyForm.getState(), urlBase);
		if (success) {
			new HistoryChangeUtil().save(historyChangeFacade, userFacade, updateAnomalyForm.getIdUserChange(),
					current.getAnomalyFormId(), current.getAnomalyFormType(), "update-state", oldState, updateAnomalyForm.getState());
			return new DataResponse(DataResponse.SUCCESS_STATE);
		}
		return new DataResponse(DataResponse.ERROR_STATE + " " + anomalyFormController.getMsg());
	}

	@GET
	@Path("/anomaly-form/get-by-id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public AnomalyFormDetail getAnomaliesFormById(@PathParam("id") Integer id) {
		AnomalyForm anomalyForm = (AnomalyForm) anomalyFormFacade.find(id);
		if ( anomalyForm == null ) {
			System.out.println("El formId no existe: " + id);
			return new AnomalyFormDetail();
		}
		return new AnomalyFormDetail(anomalyForm, userFacade, organizationsUserFacade);
	}

	@GET
	@Path("/anomaly-form/get-by-type/{type}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AnomalyFormDetail> getAnomaliesFormByType(@PathParam("type") String type) {
		List<AnomalyForm> anomaliesForms = anomalyFormFacade.findByType(type);
		List<AnomalyFormDetail> anomaliesFormDetail = new ArrayList<>();
		for(AnomalyForm anomalyForm : anomaliesForms) {
			anomaliesFormDetail.add(new AnomalyFormDetail(anomalyForm, userFacade, organizationsUserFacade));
		}
		return anomaliesFormDetail;
	}

	@GET
	@Path("/anomaly-form/get-by-type-pin/{type}/{pin}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AnomalyFormDetail> getAnomaliesFormByType(@PathParam("type") String type, @PathParam("pin") String pin) {
		List<AnomalyFormDetail> anomaliesFormDetail = new ArrayList<>();
		AllowedUser allowedUser = allowedUserFacade.findByUserPin(pin);
		if (allowedUser == null) return anomaliesFormDetail;
		List<AnomalyForm> anomaliesForms = getAnomalies(allowedUser, type);
		for(AnomalyForm anomalyForm : anomaliesForms) {
			anomaliesFormDetail.add(new AnomalyFormDetail(anomalyForm, userFacade, organizationsUserFacade));
		}
		return anomaliesFormDetail;
	}

	@GET
	@Path("/anomaly-form/get-all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AnomalyFormDetail> getAnomaliesFormAll() {
		List<AnomalyForm> anomaliesForms = anomalyFormFacade.findAll();
		List<AnomalyFormDetail> anomaliesFormDetail = new ArrayList<>();
		for(AnomalyForm anomalyForm : anomaliesForms) {
			anomaliesFormDetail.add(new AnomalyFormDetail(anomalyForm, userFacade, organizationsUserFacade));
		}
		return anomaliesFormDetail;
	}

	@GET
	@Path("/anomaly-form/get-summary")
	@Produces(MediaType.APPLICATION_JSON)
	@SuppressWarnings("unchecked")
	public List<AnomalySummary> getAnomaliesFormSummary() {
		List<Object[]> resultList = (List<Object[]>) anomalyFormFacade.summary();
		List<AnomalySummary> summaries = new ArrayList<>();
		for ( Object[] arr : resultList ) {
			summaries.add(new AnomalySummary((String) arr[0], (Long) arr[1]));
		}
		return summaries;
	}

	@GET
	@Path("/anomaly-form/get-summary-pin/{pin}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AnomalySummary> getAnomaliesFormSummaryByPin(@PathParam("pin") String pin) {
		List<AnomalySummary> summaries = new ArrayList<>();
		AllowedUser allowedUser = allowedUserFacade.findByUserPin(pin);
		if (allowedUser == null) return summaries;
		List<Object[]> resultList = getSummaries(allowedUser);
		for ( Object[] arr : resultList ) {
			summaries.add(new AnomalySummary((String) arr[0], (Long) arr[1]));
		}
		return summaries;
	}

	private List<AnomalyForm> getAnomalies(AllowedUser allowedUser, String type) {
		String organizationManglarType = allowedUser.getOrganizationManglar().getOrganizationManglarType();
		if ("super-admin".equals(organizationManglarType)) {
			return anomalyFormFacade.findByType(type);
		}
		String organizationManglarName = allowedUser.getOrganizationManglar().getOrganizationManglarName();
		List<String> types = getTypes(organizationManglarName);
		if ( types.indexOf(type) < 0) return new ArrayList<AnomalyForm>();

		String province = allowedUser.getGeographicalLocation().getGeloName();
		return anomalyFormFacade.findByTypeProvince(type, province);
	}

	@SuppressWarnings({ "unchecked" })
	private List<Object[]> getSummaries(AllowedUser allowedUser){
		String organizationManglarType = allowedUser.getOrganizationManglar().getOrganizationManglarType();
		if ("super-admin".equals(organizationManglarType)) {
			return (List<Object[]>) anomalyFormFacade.summary();
		}
		String organizationManglarName = allowedUser.getOrganizationManglar().getOrganizationManglarName();
		List<String> types = getTypes(organizationManglarName);
		String province = allowedUser.getGeographicalLocation().getGeloName();
		return (List<Object[]>) anomalyFormFacade.summaryByTypeAndProvince(types, province);
	}

	private List<String> getTypes(String organizationName){
		switch(organizationName){
			case "MAE":
				return Arrays.asList(new String[]{"tala", "contaminacion", "invasiones", "vida_silvestre", "trafico_especies"});
			case "MPCEIP":
				return Arrays.asList(new String[]{"tecnicas_pesca", "epoca_veda", "tallas_minimas"});
			case "FISCALIA":
				return Arrays.asList(new String[]{"tala", "contaminacion", "invasiones", "trafico_especies", "delincuencia_maritima"});
		}
		return new ArrayList<String>();
	}

}


class UpdateAnomalyForm{
	@Getter
    @Setter
	private Integer id;
	@Getter
    @Setter
	private String state;
	@Getter
    @Setter
	private Integer idUserChange;
}

class AnomalySummary{
	@Getter
    @Setter
	private String id;
	@Getter
    @Setter
	private long total;
	public AnomalySummary(String id, long total) {
		super();
		this.id = id;
		this.total = total;
	}
}

class AnomalyFormDetail {
	
	public AnomalyFormDetail(AnomalyForm anomalyForm, UserFacade userFacade, OrganizationsUserFacade organizationsUserFacade){
		User user = (User) userFacade.find(anomalyForm.getUserId());		
		anomalyFormId = anomalyForm.getAnomalyFormId();
		userId = anomalyForm.getUserId();
		anomaliesOffenders = anomalyForm.getAnomaliesOffenders();
		anomaliesWitnesses = anomalyForm.getAnomaliesWitnesses();
		anomaliesEvidences = anomalyForm.getAnomaliesEvidences();
		anomalyFormType = AnomalyFormController.getTypeName(anomalyForm.getAnomalyFormType());
		anomalyFormTypeId = anomalyForm.getAnomalyFormType();
		anomalyFormStatus = anomalyForm.getAnomalyFormStatus();
		anomalyFormState = anomalyForm.getAnomalyFormState();
		createdAt = anomalyForm.getCreatedAt();
		area = anomalyForm.getArea();
		industrialType = anomalyForm.getIndustrialType();
		artesanalType = anomalyForm.getArtesanalType();
		fishingType = anomalyForm.getFishingType();
		fishingTypeOthers = anomalyForm.getFishingTypeOthers();
		date = anomalyForm.getDate();
		beachingReason = anomalyForm.getBeachingReason();
		animalsStrandedCount = anomalyForm.getAnimalsStrandedCount();
		animalsStrandedSize = anomalyForm.getAnimalsStrandedSize();
		seaConditions = anomalyForm.getSeaConditions();
		anomalyFormSubtype = anomalyForm.getAnomalyFormSubtype();
		type = anomalyForm.getType();
		custody = anomalyForm.getCustody();
		protectedArea = anomalyForm.getProtectedArea();
		description = anomalyForm.getDescription();
		individualsRequisitionedInfo = anomalyForm.getIndividualsRequisitionedInfo();
		individuals = anomalyForm.getIndividuals();
		individualsRequisitioned = anomalyForm.getIndividualsRequisitioned();
		province = anomalyForm.getProvince();
		canton = anomalyForm.getCanton();
		location = anomalyForm.getLocation();
		latlong = anomalyForm.getLatlong();
		address = anomalyForm.getAddress();
		estuary = anomalyForm.getEstuary();
		community = anomalyForm.getCommunity();
		feelDanger = anomalyForm.getFeelDanger();
		hideIdentity = anomalyForm.getHideIdentity();

		// Fill user and organizationManglarInfo
		if ( user == null || user.getPeople() == null ) {
			System.out.println("No se obtuvo la información del usuario " + anomalyForm.getUserId() + " para el formId: " + anomalyForm.getAnomalyFormId());
			return;
		}
		userName = user.getPeople().getPeopName();
		userPin = user.getUserName();
		userEmail = user.getPeople().getEmail();
		userPhone = user.getPeople().getPhone();
		List<OrganizationManglar> organizationsManglar = organizationsUserFacade.findByUserId(user.getUserId()); 
		if (organizationsManglar.size() == 0) {
			System.out.println("No se obtuvo la información de organización " + anomalyForm.getUserId() + " para el formId: " + anomalyForm.getAnomalyFormId());
			return;
		}
		OrganizationManglar organizationManglar = organizationsManglar.get(0);
		userOrganizationManglarId = organizationManglar.getOrganizationManglarId();
		userOrganizationManglarName = organizationManglar.getOrganizationManglarName();
	}

	public AnomalyFormDetail() {
	}

	@Getter
	@Setter	
	private Integer anomalyFormId;

	@Getter
	@Setter	
	private Integer userId;

	@Getter
	@Setter	
	private List<AnomalyOffender> anomaliesOffenders;

	@Getter
	@Setter	
	private List<AnomalyWitness> anomaliesWitnesses;

	@Getter
	@Setter	
	private List<AnomalyEvidence> anomaliesEvidences;

	@Getter
	@Setter	
	private String anomalyFormType;

	@Getter
	@Setter	
	private String anomalyFormTypeId;

	@Getter
	@Setter	
	private Boolean anomalyFormStatus;

	@Getter
	@Setter	
	private String anomalyFormState;

	@Getter
	@Setter	
	private Date createdAt;

	@Getter
	@Setter	
	private String area;

	@Getter
	@Setter	
	private String industrialType;

	@Getter
	@Setter	
	private String artesanalType;

	@Getter
	@Setter	
	private String fishingType;

	@Getter
	@Setter	
	private String fishingTypeOthers;

	@Getter
	@Setter	
	private Date date;

	@Getter
	@Setter	
	private String beachingReason;

	@Getter
	@Setter	
	private Integer animalsStrandedCount;

	@Getter
	@Setter	
	private String animalsStrandedSize;

	@Getter
	@Setter	
	private String seaConditions;

	@Getter
	@Setter	
	private String anomalyFormSubtype;

	@Getter
	@Setter	
	private String type;

	@Getter
	@Setter	
	private String custody;

	@Getter
	@Setter	
	private String protectedArea;

	@Getter
	@Setter	
	private String description;

	@Getter
	@Setter	
	private String individualsRequisitionedInfo;

	@Getter
	@Setter	
	private String individuals;

	@Getter
	@Setter	
	private String individualsRequisitioned;

	@Getter
	@Setter	
	private String province;

	@Getter
	@Setter	
	private String canton;

	@Getter
	@Setter	
	private String location;

	@Getter
	@Setter	
	private String latlong;

	@Getter
	@Setter	
	private String address;

	@Getter
	@Setter	
	private String estuary;

	@Getter
	@Setter	
	private String community;

	@Getter
	@Setter	
	private Boolean feelDanger;

	@Getter
	@Setter	
	private Boolean hideIdentity;

	@Getter
	@Setter
	private String userName;

	@Getter
	@Setter
	private String userPin;

	@Getter
	@Setter
	private String userPhone;

	@Getter
	@Setter
	private String userEmail;

	@Getter
	@Setter
	private Integer userOrganizationManglarId;

	@Getter
	@Setter
	private String userOrganizationManglarName;
}