package ec.gob.ambiente.api.resource;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.captcha.botdetect.web.servlet.SimpleCaptcha;

import ec.gob.ambiente.api.model.DataResponse;

@Path("/")
public class CaptchaResource {

	@POST
	@Path("/captcha-validate")
	@Produces(MediaType.APPLICATION_JSON)
	public DataResponse upload(@Context HttpServletRequest servletRequest) {
        System.out.println("llego");
		try{
			JsonParser parser = new JsonParser();
	        JsonObject formDataObj = (JsonObject) parser.parse(servletRequest.getReader());
	        String userEnteredCaptchaCode = formDataObj.get("userEnteredCaptchaCode").getAsString();
	        String captchaId = formDataObj.get("captchaId").getAsString();
			SimpleCaptcha captcha = SimpleCaptcha.load(servletRequest);
	        boolean success = captcha.validate(userEnteredCaptchaCode, captchaId);
			if (success) {
				return new DataResponse(DataResponse.SUCCESS_STATE);
			}
		} catch(Exception e) {
	        System.out.println(e.getMessage());
		}
		return new DataResponse(DataResponse.ERROR_STATE);
	}
}
