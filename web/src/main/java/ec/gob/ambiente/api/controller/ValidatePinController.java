package ec.gob.ambiente.api.controller;

import lombok.Getter;
import lombok.Setter;
import ec.gob.ambiente.enlisy.model.GeographicalLocation;
import ec.gob.ambiente.enlisy.model.Role;
import ec.gob.ambiente.enlisy.services.RoleFacade;
import ec.gob.ambiente.enlisy.services.UserFacade;
import ec.gob.ambiente.manglarapp.model.AllowedUser;
import ec.gob.ambiente.manglarapp.model.OrganizationManglar;
import ec.gob.ambiente.manglarapp.services.AllowedUserFacade;

public class ValidatePinController {
	
	@Getter
    @Setter
    private String msg;

	@Getter
    @Setter
    private String userName;

	@Getter
    @Setter
	private Role role;

	@Getter
    @Setter
	private OrganizationManglar organizationManglar;

	@Getter
    @Setter
	private GeographicalLocation geographicalLocation;

    private UserFacade userFacade;
    private RoleFacade roleFacade;
	private AllowedUserFacade allowedUserFacade;

	public ValidatePinController(UserFacade userFacade,
			AllowedUserFacade allowedUserFacade,
			RoleFacade roleFacade) {
		super();
		this.userFacade = userFacade;
		this.allowedUserFacade = allowedUserFacade;
		this.roleFacade = roleFacade;
	}

	public boolean validateSocio(String userPin){
		return validate(userPin, SocioManglarController.ROL_SOCIO_MANGLAR);
	}

	public boolean validateSocioAdmin(String userPin){
		return validate(userPin, SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR);
	}

	private boolean validate(String userPin, String roleName) {

		if (userPin == null || userPin.isEmpty() || userPin.length() != 10){
			msg = "Debe ingresar una cédula valida";
			return false;			
		}

		AllowedUser allowedUser = allowedUserFacade.findByUserPin(userPin);
		boolean userIsNotMember = allowedUser == null;
		if ( userIsNotMember ) {
			msg = "El usuario no es socio de organización";
			return false;
		}

		// Validate if user exists
		boolean userExists = userFacade.findByUserName(userPin).getUserId() != null;
		if ( userExists ) {
			msg = "El usuario ya se encuentra registrado";
			return false;
		}

		boolean passRole = passRole(allowedUser.getRole().getRoleId(), roleName);
    	if( !passRole ) {
			msg = "El usuario no tiene asignados los perfiles para este sistema. Por favor comuníquese con Mesa de Ayuda.";
			return false;
		}

		// SUCCESS
		userName = allowedUser.getAllowedUserName();
		organizationManglar = allowedUser.getOrganizationManglar();
		geographicalLocation = allowedUser.getGeographicalLocation();
		return true;
	}

	public boolean passRole(Integer roleId, String roleName){
    	role = (Role) roleFacade.find(roleId);
    	if (role == null) return false;
    	
    	boolean isSuperAdmin = role.getRoleName().equals(SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR_SUPER);
    	boolean askForAdmin = roleName.equals(SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR);
    	if ( isSuperAdmin && askForAdmin ) return true;

    	return role.getRoleName().equals(roleName);
	}

}
