package ec.gob.ambiente.api.resource;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lombok.Getter;
import lombok.Setter;
import ec.gob.ambiente.api.model.DataResponse;
import ec.gob.ambiente.enlisy.model.Contact;
import ec.gob.ambiente.enlisy.model.ContactsForm;
import ec.gob.ambiente.enlisy.model.GeographicalLocation;
import ec.gob.ambiente.enlisy.model.Nationality;
import ec.gob.ambiente.enlisy.model.People;
import ec.gob.ambiente.enlisy.model.User;
import ec.gob.ambiente.enlisy.services.ContactFacade;
import ec.gob.ambiente.enlisy.services.GeographicalLocationFacade;
import ec.gob.ambiente.enlisy.services.NationalityFacade;
import ec.gob.ambiente.enlisy.services.PeopleFacade;
import ec.gob.ambiente.enlisy.services.UserFacade;
import ec.gob.ambiente.suia.utils.JsfUtil;

@Path("/user-profile")
public class UserProfileResource {

	@EJB
	private UserFacade userFacade;

	@EJB
	private PeopleFacade peopleFacade;

	@EJB
	private ContactFacade contactFacade;

	@EJB
	private GeographicalLocationFacade geographicalLocationFacade;

	@EJB
	private NationalityFacade nationalityFacade;

	@GET
	@Path("/get/{user-pin}")
	@Produces(MediaType.APPLICATION_JSON)
	public UserProfile get(@PathParam("user-pin") String userPin) {
		User user = userFacade.findByUserName(userPin);
		if (user == null) return new UserProfile();
		return new UserProfile(user);
	}

	@POST
	@Path("/save")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataResponse save(UserProfile u) {

		String errorMsg = validarRegistro(u.getPin(), u.getEmail(), u.getMobile(), u.getPhone());
		if (errorMsg != null) return new DataResponse(DataResponse.ERROR_STATE + " " + errorMsg);

		User user = userFacade.findByUserName(u.getPin());
		People people = user.getPeople();

		Contact phone = people.getContact(ContactsForm.TELEFONO);
		phone.setValue(u.getPhone());
		contactFacade.save(phone);

		Contact mobile = people.getContact(ContactsForm.CELULAR);
		mobile.setValue(u.getMobile());
		contactFacade.save(mobile);

		Contact email = people.getContact(ContactsForm.EMAIL);
		email.setValue(u.getEmail());
		contactFacade.save(email);

		Contact address = people.getContact(ContactsForm.DIRECCION);
		address.setValue(u.getAddress());
		contactFacade.save(address);

		Nationality nationality = nationalityFacade.findById(u.getNationalityId());
		people.setNationality(nationality);

		GeographicalLocation geographicalLocation = geographicalLocationFacade.findById(u.getParroquiaId());
		people.setGeographicalLocation(geographicalLocation);

		boolean success = peopleFacade.save(people);
		if (success) {
			return new DataResponse(DataResponse.SUCCESS_STATE);
		}
		return new DataResponse(DataResponse.ERROR_STATE + " No se guardo correctamente");
	}

	private String validarRegistro(String pin, String email, String mobile, String phone) {
		String errorMsg = null;
		  if (!JsfUtil.validarMail(email)) {
			  errorMsg = "La dirección de correo ingresada no es válida, por favor verifique.";
		  }

		  if (mobile.length() != 10) {
			  errorMsg = "El número celular ingresado no es válido, por favor verifique";
		  }

		  if (phone.length() < 9) {
			  errorMsg = "El número fijo ingresado no es válido, por favor verifique";
		  }
		  return errorMsg;
	}
}

class UserProfile {
	@Getter
	@Setter
	private String pin;
	@Getter
	@Setter
	private String name;
	@Getter
	@Setter
	private Integer nationalityId;
	@Getter
	@Setter
	private String phone;
	@Getter
	@Setter
	private String mobile;
	@Getter
	@Setter
	private String email;
	@Getter
	@Setter
	private String address;
	@Getter
	@Setter
	private Integer provinceId;
	@Getter
	@Setter
	private Integer cantonId;
	@Getter
	@Setter
	private Integer parroquiaId;

	public UserProfile(User user) {
		this.pin = user.getUserName();
		this.name = user.getPeople().getPeopName();
		this.nationalityId = user.getPeople().getNationality().getNatiId();
		this.phone = user.getPeople().getPhoneFijo();
		this.mobile = user.getPeople().getPhone();
		this.email = user.getPeople().getEmail();
		this.address = user.getPeople().getAddress();
		try {
			this.parroquiaId = user.getPeople().getGeographicalLocation().getGeloId();
			this.cantonId = user.getPeople().getGeographicalLocation().getGeographicalLocation().getGeloId();
			this.provinceId = user.getPeople().getGeographicalLocation().getGeographicalLocation().getGeographicalLocation().getGeloId();
		} catch(Exception e) {
			System.out.print("ERROR GETTING LOCATIONS " + e);
		}
	}

	public UserProfile() {
	}

}
