package ec.gob.ambiente.api.resource;

import java.io.File;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import ec.gob.ambiente.api.controller.PdfReportController;
import ec.gob.ambiente.enlisy.services.UserFacade;
import ec.gob.ambiente.manglarapp.services.AnomalyFormFacade;
import ec.gob.ambiente.manglarapp.services.OrganizationsUserFacade;

@Path("/pdf")
public class PdfReportResource {

	@EJB
	private UserFacade userFacade;

	@EJB
	private AnomalyFormFacade anomalyFormFacade;

	@EJB
	private OrganizationsUserFacade organizationsUserFacade;

	@GET
	@Path("/form/{form-id}")
	@Produces("application/pdf")
	public Response getImageFile(@PathParam("form-id") Integer formId) {
		PdfReportController pdfReportController = new PdfReportController(anomalyFormFacade, userFacade, organizationsUserFacade);
		File file = pdfReportController.getFormFile(formId);
		if (!file.exists()) {
	        ResponseBuilder rBuild = Response.status(Response.Status.BAD_REQUEST);
	        String msgError = "Recurso no disponible";
	        return rBuild.type(MediaType.TEXT_PLAIN).entity(msgError).build();
		}
		ResponseBuilder response = Response.ok((Object) file);
		//response.header("Content-Disposition", "attachment; filename=\"" + nameFile + ".pdf\"");
		return response.build();
	}

}
