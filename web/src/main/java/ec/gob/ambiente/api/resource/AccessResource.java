package ec.gob.ambiente.api.resource;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lombok.Getter;
import lombok.Setter;
import ec.gob.ambiente.api.controller.AccessController;
import ec.gob.ambiente.api.model.DataResponse;
import ec.gob.ambiente.enlisy.model.User;
import ec.gob.ambiente.enlisy.services.RoleFacade;
import ec.gob.ambiente.enlisy.services.RolesUserFacade;
import ec.gob.ambiente.enlisy.services.UserFacade;

@Path("/login")
public class AccessResource {

	@EJB
	private UserFacade userFacade;

	@EJB
	private RoleFacade roleFacade;
	
	@EJB
	private RolesUserFacade rolesUserFacade;

	@POST
	@Path("/access")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataUserAccess accessSocio(UserAccess userAccess) {
		AccessController accessController = new AccessController(userFacade, roleFacade, rolesUserFacade);
		boolean success = accessController.accederSocio(userAccess.getUsername(), userAccess.getPassword());
		if (success) {
			User user = accessController.getUser();
			String completeUserName = user.getPeople().getPeopName();
			return new DataUserAccess(DataResponse.SUCCESS_STATE, user.getUserId(), completeUserName, user.getUserPin(), user.getPeople().getEmail(), false);
		}
		return new DataUserAccess(DataResponse.ERROR_STATE + " " + accessController.getMsg(), null, null, null, null, null);
	}
	
	@POST
	@Path("/access-admin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataUserAccess accessAdmin(UserAccess userAccess) {
		AccessController accessController = new AccessController(userFacade, roleFacade, rolesUserFacade);
		boolean success = accessController.accederAdmin(userAccess.getUsername(), userAccess.getPassword());
		if (success) {
			User user = accessController.getUser();
			String completeUserName = user.getPeople().getPeopName();
			boolean isSuperAdmin = accessController.isSuperAdmin();
			return new DataUserAccess(DataResponse.SUCCESS_STATE, user.getUserId(), completeUserName, user.getUserPin(), user.getPeople().getEmail(), isSuperAdmin);
		}
		return new DataUserAccess(DataResponse.ERROR_STATE + " " + accessController.getMsg(), null, null, null, null, null);
	}

}

class DataUserAccess {
	@Getter
    @Setter
	private String state;
	@Getter
    @Setter
    private Integer userId;
	@Getter
    @Setter
    private String userName;
	@Getter
    @Setter
    private String userEmail;
	@Getter
    @Setter
    private String userPin;
	@Getter
    @Setter
    private Boolean isSuperAdmin;
	public DataUserAccess(String state, Integer userId, String userName,
			String userPin, String userEmail, Boolean isSuperAdmin) {
		super();
		this.state = state;
		this.userId = userId;
		this.userName = userName;
		this.userPin = userPin;
		this.userEmail = userEmail;
		this.isSuperAdmin = isSuperAdmin;
	}
}

class UserAccess {
	@Getter
    @Setter
	private String username, password;
}
