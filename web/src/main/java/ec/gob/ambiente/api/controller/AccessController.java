package ec.gob.ambiente.api.controller;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import ec.gob.ambiente.enlisy.model.Role;
import ec.gob.ambiente.enlisy.model.RolesUser;
import ec.gob.ambiente.enlisy.model.User;
import ec.gob.ambiente.enlisy.services.RoleFacade;
import ec.gob.ambiente.enlisy.services.RolesUserFacade;
import ec.gob.ambiente.enlisy.services.UserFacade;
import ec.gob.ambiente.enums.TipoUsuarioEnum;
import ec.gob.ambiente.suia.utils.JsfUtil;

public class AccessController {
	
	@Getter
	@Setter
	private String msg;
	
	@Getter
	@Setter
	private User user;
	
	@Getter
	@Setter
	private boolean isSuperAdmin;
	
	private List<RolesUser> rolesUsers = new ArrayList<RolesUser>();
	
	private UserFacade userFacade;
	private RoleFacade roleFacade;
	private RolesUserFacade rolesUserFacade;

	public AccessController(UserFacade userFacade, RoleFacade roleFacade,
			RolesUserFacade rolesUserFacade) {
		super();
		this.userFacade = userFacade;
		this.roleFacade = roleFacade;
		this.rolesUserFacade = rolesUserFacade;
	}

	public boolean accederSocio(String username, String password) {
		return acceder(username, password, SocioManglarController.ROL_SOCIO_MANGLAR);
	}
	
	public boolean accederAdmin(String username, String password) {
		boolean isAdmin = acceder(username, password, SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR_SUPER);
		if (isAdmin) return true;
		return acceder(username, password, SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR);
	}
	
	private boolean acceder(String username, String password, String roleName) {
		try {
			boolean successValidate = verificarUsuario(username, password);
			if (!successValidate) {
				return false;
			}
			
			// Validate roles
			rolesUsers = rolesUserFacade.findByUserNameAndRoleName(username, roleName);
			if( rolesUsers == null ) {
				msg = "El usuario no tiene asignados los perfiles para este sistema. Por favor comuníquese con Mesa de Ayuda.";
				return false;
			}
			isSuperAdmin = isSuperAdmin(rolesUsers);
			return true;
		} catch (IllegalStateException e) {
			msg = "El usuario no tiene asignados los perfiles para este sistema. Por favor comuníquese con Mesa de Ayuda.";	
		}
		return false;
	}
	
	private boolean isSuperAdmin(List<RolesUser> rolesUsers){
		boolean isSuperAdmin = false;
		for (RolesUser rolesUser : rolesUsers ) {
			if (rolesUser.getRole().getRoleName().equals(SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR_SUPER)){
				isSuperAdmin = true;
			}
		}
		return isSuperAdmin;
	}
	
	public boolean verificarUsuario(String username, String password) {
		
		user = userFacade.findByUserName(username);
		
		if(user.getUserId() == null) {
			msg = "Usuario no encontrado: " + username;
			user=userFacade.findByUserNameDisabled(username);
			if(user.getUserId() != null && (user.getUserStatus()==null || !user.getUserStatus())) {
				msg = "El usuario (" + username + ") se encuentra desactivado. Por favor comuniquese con Mesa de Ayuda (maetransparente@ambiente.gob.ec)";			
			}
			return false;
		}
		// si el usuario está desactivado
		if(user.getUserStatus()==null || !user.getUserStatus()) {
			msg = "El usuario (" + username + ") se encuentra desactivado. Por favor comuniquese con Mesa de Ayuda (maetransparente@ambiente.gob.ec)";
			return false;
		}
		
		try {// If para verificar si la clave ingresada coincide con la clave de
			// bdd
			if (!user.getUserPassword().equals(JsfUtil.claveEncriptadaSHA1(password))
				&& !(user.getUserPassword().equals(password) && password.length() == 40)) {
				msg = "Clave de Usuario (" + username + ") Incorrecta: ";
				return false;				
			}
		} catch (Exception e) {
			e.printStackTrace();
			msg = "Error Password: " + e.getMessage();
			return false;				
		}
		
		try {
			for (TipoUsuarioEnum tipoUsuario : TipoUsuarioEnum.values()) {
				Role role = roleFacade.findByName(JsfUtil.getProperty(tipoUsuario.getCodigo(),true));
				if (role != null) {		
					List<RolesUser> rolesUsuario = rolesUserFacade.findByUserNameAndRoleName(user.getUserName(),role.getRoleName());
					if (rolesUsuario!=null) {
						return true;
					} else {
						msg = "El usuario (" + username + ") no tiene el perfil para el ingreso al sistema. Por favor comuniquese con Mesa de Ayuda (maetransparente@ambiente.gob.ec)";
					}
					
				}else{
					msg = "Rol no existe: "+JsfUtil.getProperty(tipoUsuario.getCodigo(),true);
				}
			}			
			msg = "El usuario (" + username + ") no tiene el perfil para el ingreso al sistema. Por favor comuniquese con Mesa de Ayuda (maetransparente@ambiente.gob.ec)";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "Error Rol: " + e.getMessage();
		}
		return false;
	}
	
}
