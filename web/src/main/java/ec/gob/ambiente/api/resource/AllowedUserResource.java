package ec.gob.ambiente.api.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import lombok.Getter;
import lombok.Setter;
import ec.gob.ambiente.api.controller.AllowedUserController;
import ec.gob.ambiente.api.controller.SocioManglarController;
import ec.gob.ambiente.api.model.DataResponse;
import ec.gob.ambiente.enlisy.services.GeographicalLocationFacade;
import ec.gob.ambiente.enlisy.services.RoleFacade;
import ec.gob.ambiente.manglarapp.model.AllowedUser;
import ec.gob.ambiente.manglarapp.services.AllowedUserFacade;
import ec.gob.ambiente.manglarapp.services.OrganizationManglarFacade;

@Path("/allowed-user")
public class AllowedUserResource {

	@EJB
	private AllowedUserFacade allowedUserFacade;

	@EJB
	private OrganizationManglarFacade organizationManglarFacade;

	@EJB
	private GeographicalLocationFacade geographicalLocationFacade;

	@EJB
	private RoleFacade roleFacade;

	@POST
	@Path("/save-socio")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataResponse saveSocio(TransferAllowedUser transferAllowedUser) {
		AllowedUserController allowedUserController = new AllowedUserController(allowedUserFacade, roleFacade, organizationManglarFacade, geographicalLocationFacade);
		boolean success = allowedUserController.save(
				transferAllowedUser.getAllowedUser(),
				SocioManglarController.ROL_SOCIO_MANGLAR,
				transferAllowedUser.getOrganizationManglarId(),
				transferAllowedUser.getGeloId());
		if (success) {
			return new DataResponse(DataResponse.SUCCESS_STATE);
		}
		return new DataResponse(DataResponse.ERROR_STATE + " " + allowedUserController.getMsg());
	}

	@POST
	@Path("/save-admin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataResponse saveAdmin(TransferAllowedUser transferAllowedUser) {
		AllowedUserController allowedUserController = new AllowedUserController(allowedUserFacade, roleFacade, organizationManglarFacade, geographicalLocationFacade);
		boolean success = allowedUserController.save(
				transferAllowedUser.getAllowedUser(),
				SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR,
				transferAllowedUser.getOrganizationManglarId(),
				transferAllowedUser.getGeloId());
		if (success) {
			return new DataResponse(DataResponse.SUCCESS_STATE);
		}
		return new DataResponse(DataResponse.ERROR_STATE + " " + allowedUserController.getMsg());
	}

	@POST
	@Path("/save-super-admin")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public DataResponse saveSuperAdmin(TransferAllowedUser transferAllowedUser) {
		AllowedUserController allowedUserController = new AllowedUserController(allowedUserFacade, roleFacade, organizationManglarFacade, geographicalLocationFacade);
		boolean success = allowedUserController.save(
				transferAllowedUser.getAllowedUser(),
				SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR_SUPER,
				transferAllowedUser.getOrganizationManglarId(),
				transferAllowedUser.getGeloId());
		if (success) {
			return new DataResponse(DataResponse.SUCCESS_STATE);
		}
		return new DataResponse(DataResponse.ERROR_STATE + " " + allowedUserController.getMsg());
	}

	@GET
	@Path("/get-socio")
	@Produces(MediaType.APPLICATION_JSON)
	public List<InfoAllowedUser> getSocio() {
		List<InfoAllowedUser> info = new ArrayList<>();
		List<AllowedUser> allowedUsers = allowedUserFacade
			.findByRoleNameIncludeAllStatus(SocioManglarController.ROL_SOCIO_MANGLAR);
		for ( AllowedUser allowedUser : allowedUsers ) {
			info.add(new InfoAllowedUser(allowedUser));
		}
		return info;
	}

	@GET
	@Path("/get-admin")
	@Produces(MediaType.APPLICATION_JSON)
	public List<InfoAllowedUser> getAdmin() {
		List<InfoAllowedUser> info = new ArrayList<>();
		List<AllowedUser> allowedUsers = allowedUserFacade
			.findByRoleNameIncludeAllStatus(SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR);
		for ( AllowedUser allowedUser : allowedUsers ) {
			info.add(new InfoAllowedUser(allowedUser));
		}
		return info;
	}

	@GET
	@Path("/get-super-admin")
	@Produces(MediaType.APPLICATION_JSON)
	public List<InfoAllowedUser> getSuperAdmin() {
		List<InfoAllowedUser> info = new ArrayList<>();
		List<AllowedUser> allowedUsers = allowedUserFacade
			.findByRoleNameIncludeAllStatus(SocioManglarController.ROL_SOCIO_MANGLAR_ADMINISTRADOR_SUPER);
		for ( AllowedUser allowedUser : allowedUsers ) {
			info.add(new InfoAllowedUser(allowedUser));
		}
		return info;
	}

	@POST
	@Path("/remove")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public DataResponse remove(DataRemove dataRemove) {
		AllowedUser allowedUser = allowedUserFacade.find(dataRemove.getId());
		allowedUserFacade.remove(allowedUser);
		return new DataResponse(DataResponse.SUCCESS_STATE);
	}

}

class TransferAllowedUser {
	@Getter
	@Setter
	private Integer organizationManglarId;
	@Getter
	@Setter
	private Integer geloId;
	@Getter
	@Setter
	private AllowedUser allowedUser;
}

class InfoAllowedUser {
	@Getter
	@Setter
	private Integer allowedUserId;
	@Getter
	@Setter
	private Boolean allowedUserStatus;
	@Getter
	@Setter
	private Integer geloId;
	@Getter
	@Setter
	private Integer organizationManglarId;
	@Getter
	@Setter
	private String organizationManglarName;
	@Getter
	@Setter
	private String geographicalLocationName;
	@Getter
	@Setter
	private String allowedUserName;
	@Getter
	@Setter
	private String allowedUserPin;
	public InfoAllowedUser(AllowedUser allowedUser) {
		super();
		this.allowedUserId = allowedUser.getAllowedUserId();
		this.allowedUserStatus = allowedUser.getAllowedUserStatus();
		this.organizationManglarName = allowedUser.getOrganizationManglar().getOrganizationManglarName();
		this.geographicalLocationName = allowedUser.getGeographicalLocation().getGeloName();
		this.allowedUserName = allowedUser.getAllowedUserName();
		this.allowedUserPin = allowedUser.getAllowedUserPin();
		this.geloId = allowedUser.getGeographicalLocation().getGeloId();
		this.organizationManglarId = allowedUser.getOrganizationManglar().getOrganizationManglarId();
	}
}
